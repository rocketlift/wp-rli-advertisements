<?php
/*
Plugin Name: Rocket Lift Ads
Version: 1.0
Description: A better advertising plugin than any we could find.
Author: Matthew Eppelsheimer
Author URI: https://rocketlift.com
Plugin URI: https://rocketlift.com
Text Domain: rli-ads
*/

if ( ! class_exists( 'RLI_Advertisement_Post_Type' ) ) {
	// require_once( 'library/class-advertisement-post-type.php' );
}



/**
 * Register ad banner image sizes
 *
 * @uses add_image_size( $name, $width, $height, $crop )
 *
 */
function rli_ad_sizes(){
	/**
	 * Filter default image sizes the plugin registers
	 */
	$sizes = apply_filters( 'rli_ad_filter_image_sizes', array(
		array( 'ad-banner-thin-medium', 465, 85, false )
	) );

	foreach ( $sizes as $size ) {
		add_image_size( $size[0], $size[1], $size[2], $size[3] );
	}
}
add_action( 'init', 'rli_ad_sizes' );

/**
 * A template tag that echoes HTML and JavaScript to render advertisements.
 *
 * Currently (1.0) echoes out all ads.
 *
 * @todo support passing a category to $ads
 * @todo support one or more ID to $ads
 *
 * @param $ads currently does nothing
 */
function rli_show_ads( $number = 1, $method = 'random', $which_ones = 'all' ) {

	// Query to get ads


	// Loop over the query to get the
	echo _rli_ad_helper();
}
/**
 *
 * Return an array of values to render ads
 * Currently does nothing
 */
function _rli_ad_helper( $ad_query_args = ''  ) {

}