<?php
/**
 * The Rocket Lift Advertisements plugin's core data type
 *
 * All functions are static members of this class to allow for easy namespacing, 
 * so this class should not be instantiated.
 *
 * @author Matthew Eppelsheimer
 */

class RLI_Advertisement_Post_Type {

	/**
	 * Set up all the things
	 */
	static function setup() {
		self::register_shortcodes();
		self::register_advertisements();
		add_filter(
			'post_updated_messages',
			array( get_class(), 'updated_messages' )
		);
	}
	
	/**
	 * Register custom post type people
	 *
	 * @author Matthew Eppelsheimer
	 * @since 0.2
	 */
	static function register_advertisements() {
		register_post_type( 'rli-ad', array(
			'labels'            => array(
				'name'                => __( 'Advertisements', 'rocketlift-ads' ),
				'singular_name'       => __( 'Advertisement', 'rocketlift-ads' ),
				'all_items'           => __( 'Advertisements', 'rocketlift-ads' ),
				'new_item'            => __( 'New Advertisement', 'rocketlift-ads' ),
				'add_new'             => __( 'Add New', 'rocketlift-ads' ),
				'add_new_item'        => __( 'Add New Advertisement', 'rocketlift-ads' ),
				'edit_item'           => __( 'Edit Advertisement', 'rocketlift-ads' ),
				'view_item'           => __( 'View Advertisement', 'rocketlift-ads' ),
				'search_items'        => __( 'Search Advertisements', 'rocketlift-ads' ),
				'not_found'           => __( 'No Advertisements found', 'rocketlift-ads' ),
				'not_found_in_trash'  => __( 'No Advertisements found in trash', 'rocketlift-ads' ),
				'parent_item_colon'   => __( 'Parent Advertisement', 'rocketlift-ads' ),
				'menu_name'           => __( 'Advertisements', 'rocketlift-ads' ),
			),
			'public'            => true,
			'hierarchical'      => false,
			'show_ui'           => true,
			'show_in_nav_menus' => true,
			'supports'          => array( 'title', 'thumbnail' ),
			'has_archive'       => false,
			'rewrite'           => true,
			'query_var'         => false
		) );
	}

	/**
	 * Set dashboard messages for rli-ad post type
	 *
	 * @since 1.0
	 * @author Matthew Eppelsheimer
	 *
	 * @param $messages
	 *
	 * @return mixed
	 */
	static function updated_messages( $messages ) {
		global $post;

		$permalink = get_permalink( $post );

		$messages['rli-ad'] = array(
			0 => '', // Unused. Messages start at index 1.
			1 => sprintf( __('Advertisement updated. <a target="_blank" href="%s">View Advertisement</a>', 'rocketlift-ads'), esc_url( $permalink ) ),
			2 => __('Custom field updated.', 'rocketlift-ads'),
			3 => __('Custom field deleted.', 'rocketlift-ads'),
			4 => __('Advertisement updated.', 'rocketlift-ads'),
			/* translators: %s: date and time of the revision */
			5 => isset($_GET['revision']) ? sprintf( __('Advertisement restored to revision from %s', 'rocketlift-ads'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
			6 => sprintf( __('Advertisement published. <a href="%s">View Advertisement</a>', 'rocketlift-ads'), esc_url( $permalink ) ),
			7 => __('Advertisement saved.', 'rocketlift-ads'),
			8 => sprintf( __('Advertisement submitted. <a target="_blank" href="%s">Preview Advertisement</a>', 'rocketlift-ads'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
			9 => sprintf( __('Advertisement scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Advertisement</a>', 'rocketlift-ads'),
				// translators: Publish box date format, see http://php.net/date
				date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
			10 => sprintf( __('Advertisement draft updated. <a target="_blank" href="%s">Preview Advertisement</a>', 'rocketlift-ads'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		);

		return $messages;
	}

	/**
	 * Register meta box for the advertisementpost editor screen.
	 */
	public static function create_ad_metaboxes() {
		// enables third party developers to add more meta boxes
		do_action( 'rli_ad_create_metaboxes');
	}

	/**
	 * Render link meta box.
	 *
	 * Calls 'people_title_metabox_render' action.
	 */
	static function render_ad_url_field( $post ) { ?>
		<label for="url"><?php echo __( 'Link URL:', 'rli-ads' ); ?></label>
		<p>
			<input class="widefat" type="text" name="url" id="url" value="<?php echo esc_attr( get_post_meta( $post->ID, '_title', true ) ); ?>" size="30" />
		</p>
	<?php
	}
add_action( 'people_details_metabox', 'render_people_title_field');

// Add save action
	function people_title_save_hook( $post_id ) {
		if ( 'people' == get_post_type( $post_id ) )
			people_save_meta( $post_id, 'people', 'title' );
	}
add_action( 'people_save_details', 'people_title_save_hook' );

// Add people_atts hook
	function people_title_atts_hook( $arr, $id ) {
		$arr['title'] = get_post_meta( $id, '_title', true );
		return $arr;
	}
add_filter( 'people_atts', 'people_title_atts_hook', 2, 2 );
	/**
	 * Utility function to return a WP_Query object with Advertisement posts
	 *
	 * @author Matthew Eppelsheimer
	 */
	public static function query_ads( $args ) {
		$defaults = array(
			'posts_per_page' => - 1,
			'order'          => 'ASC',
			'orderby'        => 'menu_order'
		);

		$query_args = wp_parse_args( $args, $defaults );
		$query_args['post_type'] = 'rli-ad';

		$results = new WP_Query( $query_args );

		return $results;
	}

	/**
	 *	Takes query arguments for ads and
	 *	performs the query, manages a custom loop, and echoes html
	 *
	 *	@param $args an array of $args formatted for WP_Query to accept
	 *	
	 *	@return true if we output html with people; false if not
	 */
	public static function render_ads( $args = null, $callback = null ) {
		global $post;
		
		if ( empty( $args['orderby'] ) ) {
			$args['orderby'] = 'menu_order';
		}
		
		$ads = self::query_ads( $args );
	
		if ( $ads->have_posts() ) {
			$out = '';
			while ( $ads->have_posts() ) {
				$ads->the_post();
			
				// BUILD HTML and JavaScript
				// Give Priority to:
				// $callback variable, 
				// then the action hook,
				// then the default method self::list_item()
				if ( $callback ) {
					$out .= $callback( $post );
				}
				elseif ( has_filter('people_item_callback' ) ) {
					$out .= apply_filters( 'people_item_callback', '' );
				}
				else {
					$out .= self::list_item();
				}
			}
			wp_reset_query();
			return $out;
		}
	
		wp_reset_query();
		return false;
	}

	/*
	 *	Shortcode Setup
	 */

	/**
	 *	Register shortcode 
	 */

	static function register_shortcodes() {
		add_shortcode( 'rli-ad', array( get_class(), 'ad_shortcode' ) );
	}

	/*
	 *	Creates a shortcode to display a list of people on demand
	 *
	 *	Supports the 'category' keyword.
	 */

	static function ad_shortcode( $atts ) {
		$atts = shortcode_atts( 
			array( 
				'category' => '',
				'orderby'  => ''
			),
			$atts
		);
		
		$query_args = array();
		
		if( $atts['category'] ) {
			$query_args['category'] = $atts['category'];
		}
		if( $atts['orderby'] ) {
			$query_args['orderby'] = $atts['orderby'];
		}
		return self::render_ads( $query_args );
	}
}

